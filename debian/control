Source: ldaptor
Section: admin
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Jan Dittberner <jandd@debian.org>
Standards-Version: 3.9.3
Build-Depends-Indep: docbook-slides (>= 3.2.0), xsltproc,
 source-highlight, python-epydoc, dia (>= 0.93-2), python-docutils,
 python-twisted-core, python-twisted-names, python-twisted-mail,
 python-pyparsing, python-openssl, python-crypto, python (>= 2.6.5~)
Build-Depends: debhelper (>= 7.0.50~)
X-Python-Version: all
Homepage: http://www.inoi.fi/open/trac/ldaptor
Vcs-Git: https://salsa.debian.org/python-team/packages/ldaptor.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/ldaptor

Package: ldaptor-utils
Architecture: all
Depends: ${python:Depends}, ${misc:Depends}, python-ldaptor
Description: command-line LDAP utilities
 A set of LDAP utilities for use from the command line, including:
 .
  * ldaptor-search -- Search LDAP directories.
  * ldaptor-namingcontexts -- Fetch the naming contexts the server supports.
  * ldaptor-find-server -- Find the server that serves the wanted DN by
    looking at DNS SRV records.
  * ldaptor-passwd -- Change passwords.
  * ldaptor-rename -- Change object RDN and DNs.
  * ldaptor-ldap2passwd -- Generate passwd(5) format data from LDAP
    accounts.
  * ldaptor-getfreenumber -- Do an efficient scan for e.g. next free
    uidNumber.
  * ldaptor-ldap2dhcpconf -- Create dhcp.conf based on LDAP host info.
  * ldaptor-ldap2dnszones -- Create a DNS zone files based on LDAP host info.
  * ldaptor-ldap2maradns -- Create a maradns zone file based on LDAP host info.
  * ldaptor-ldap2pdns -- pdns pipe backend.
  * ldaptor-fetchschema -- Fetch schema from a server.

Package: python-ldaptor
Section: python
Architecture: all
Depends: ${python:Depends}, ${misc:Depends},
 python-twisted-core (>= 2.2), python-twisted-names (>= 0.2),
 python-twisted-mail (>= 0.2), python-twisted-web (>= 0.5),
 python-pyparsing
Provides: ${python:Provides}
Breaks: ${python:Breaks}
Replaces: ldaptor-common
Conflicts: ldaptor-common
Description: pure-Python library for LDAP operations
 Ldaptor is a pure-Python library that implements:
 .
  * LDAP client logic.
  * separately-accessible LDAP and BER protocol message generation/parsing.
  * ASCII-format LDAP filter generation and parsing.
  * LDIF format data generation.
  * Samba password changing logic.

Package: ldaptor-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Description: documentation for Ldaptor
 A collection of documentation about Ldaptor and LDAP, including:
 .
  * An introduction to LDAP
  * The Ldaptor library API
  * Slides for a talk "Creating a simple LDAP application"
